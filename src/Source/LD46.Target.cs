// Copyright 2020 Andrew Joy, All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class LD46Target : TargetRules
{
	public LD46Target( TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.AddRange( new string[] { "LD46" } );
	}
}
