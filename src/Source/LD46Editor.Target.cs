// Copyright 2020 Andrew Joy, All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class LD46EditorTarget : TargetRules
{
	public LD46EditorTarget( TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.AddRange( new string[] { "LD46" } );
	}
}
