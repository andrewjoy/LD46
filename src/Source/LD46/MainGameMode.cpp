// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "MainGameMode.h"

#include "MainCharacter.h"
#include "MainPlayerController.h"

AMainGameMode::AMainGameMode()
{
	this->DefaultPawnClass = AMainCharacter::StaticClass();
	this->PlayerControllerClass = AMainPlayerController::StaticClass();
}
