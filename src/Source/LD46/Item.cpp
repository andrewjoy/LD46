// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Item.h"

#include "MainCharacter.h"

UItem::UItem()
{
	this->Name = "Item";
	this->Rarity = EItemRarity::Common;
	this->BasePrice = 100.0f;
	this->Price = -1.0f;
	this->Count = 0;
}

void UItem::TickPrice(float DeltaSeconds)
{
	if (this->Price <= 0.0f)
	{
		this->Price = this->BasePrice;
	}

	this->Price = FMath::Clamp(this->Price + ((int)this->Rarity) * DeltaSeconds * (1.0f / 3.0f), this->BasePrice * 0.8f, this->BasePrice * 1.2f);
}

void UItem::ChangeCount(int32 DeltaCount)
{
	this->Count = FMath::Max(0, this->Count + DeltaCount);
}

void UItem::Sell()
{
	if (this->Count > 0)
	{
		AMainCharacter* const MainCharacter = Cast<AMainCharacter>(this->GetWorld()->GetFirstPlayerController()->GetPawn());
		if (MainCharacter != nullptr)
		{
			MainCharacter->ChangeCoins(this->Price);

			this->ChangeCount(-1);

			this->Price *= 0.8f;
			this->TickPrice(0.0f);
		}
	}
}
