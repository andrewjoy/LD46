// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Ingredient.h"

#include "Kismet/GameplayStatics.h"

#include "Cauldron.h"

UIngredient::UIngredient()
{
	this->Name = "Ingredient";
}

void UIngredient::AddToCauldron()
{
	ACauldron* const Cauldron = Cast<ACauldron>(UGameplayStatics::GetActorOfClass(this, ACauldron::StaticClass()));
	if (Cauldron != nullptr)
	{
		if (Cauldron->AddIngredient(this->GetClass()))
		{
			this->ChangeCount(-1);
		}
	}
}
