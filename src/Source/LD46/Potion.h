// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "Item.h"

#include "Potion.generated.h"

UCLASS()
class LD46_API UPotion : public UItem
{
	GENERATED_BODY()
	
public:
	UPotion();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = Potion)
	void Use();

protected:
	virtual void Use_Implementation();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Potion)
	TArray<TSubclassOf<class UIngredient>> Ingredients;

public:
	FORCEINLINE TArray<TSubclassOf<class UIngredient>> const& GetIngredients() const { return this->Ingredients; }
};
