// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "VolatilyIngredient.h"

UVolatilyIngredient::UVolatilyIngredient()
{
	this->Name = "Volatily";
	this->Rarity = EItemRarity::Common;
}
