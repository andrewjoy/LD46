// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "../Ingredient.h"

#include "VolatilyIngredient.generated.h"

UCLASS()
class LD46_API UVolatilyIngredient : public UIngredient
{
	GENERATED_BODY()
	
public:
	UVolatilyIngredient();
};
