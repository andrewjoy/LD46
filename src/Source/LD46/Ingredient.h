// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "Item.h"

#include "Ingredient.generated.h"

UCLASS()
class LD46_API UIngredient : public UItem
{
	GENERATED_BODY()
	
public:
	UIngredient();

	UFUNCTION(BlueprintCallable, Category = Ingredient)
	void AddToCauldron();

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Ingredient)
	TArray<TSubclassOf<class UIngredient>> DangerousCombinations;

public:
	FORCEINLINE TArray<TSubclassOf<class UIngredient>> const& GetDangerousCombinations() const { return this->DangerousCombinations; }
};
