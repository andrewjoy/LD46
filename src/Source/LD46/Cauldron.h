// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/Actor.h"

#include "Cauldron.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCauldronIngredientsChanged, ACauldron*, Sender);

UCLASS()
class LD46_API ACauldron : public AActor
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	class USphereComponent* Collision;
	
public:
	ACauldron();

protected:
	virtual void BeginPlay() override;

public:
	UPROPERTY(BlueprintAssignable, Category = Inventory)
	FCauldronIngredientsChanged OnIngredientsChanged;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Cauldron)
	int32 MaxIngredients;

	UFUNCTION(BlueprintCallable, Category = Cauldron)
	bool AddIngredient(TSubclassOf<class UIngredient> IngredientClass);

	UFUNCTION(BlueprintCallable, Category = Cauldron)
	void MakePotion();

	UFUNCTION(BlueprintCallable, Category = Cauldron)
	void Explode();

private:
	UPROPERTY()
	TArray<TSubclassOf<class UPotion>> AllPotions;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Cauldron, meta = (AllowPrivateAccess = "true"))
	TArray<TSubclassOf<class UIngredient>> Ingredients;

public:
	FORCEINLINE TArray<TSubclassOf<class UIngredient>> const& GetIngredients() const { return this->Ingredients; }
};
