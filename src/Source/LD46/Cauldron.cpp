// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Cauldron.h"

#include "Components/SphereComponent.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/PlayerController.h"
#include "UObject/UObjectIterator.h"

#include "Ingredient.h"
#include "MainCharacter.h"
#include "Potion.h"

ACauldron::ACauldron()
{
	this->Collision = this->CreateDefaultSubobject<USphereComponent>("Collision");
	this->RootComponent = this->Collision;

	this->MaxIngredients = 5;
}

void ACauldron::BeginPlay()
{
	Super::BeginPlay();

	for (TObjectIterator<UClass> Class; Class; ++Class)
	{
		if (Class->IsNative()
			&& !Class->HasAnyClassFlags(CLASS_Deprecated | CLASS_NewerVersionExists)
			&& Class->IsChildOf(UPotion::StaticClass())
			&& *Class != UPotion::StaticClass())
		{
			if (*Class != nullptr)
			{
				this->AllPotions.Add(*Class);
			}
		}
	}
}

bool ACauldron::AddIngredient(TSubclassOf<class UIngredient> IngredientClass)
{
	if (this->Ingredients.Num() >= this->MaxIngredients)
	{
		return false;
	}

	for (int i = 0; i < this->Ingredients.Num(); ++i)
	{
		if (this->Ingredients[i].GetDefaultObject()->GetDangerousCombinations().Contains(IngredientClass))
		{
			this->Explode();

			return true;
		}
	}

	this->Ingredients.Add(IngredientClass);
	this->OnIngredientsChanged.Broadcast(this);

	return true;
}

void ACauldron::MakePotion()
{
	AMainCharacter* const MainCharacter = Cast<AMainCharacter>(this->GetWorld()->GetFirstPlayerController()->GetPawn());
	if (MainCharacter != nullptr)
	{
		TArray<TSubclassOf<UPotion>> ViablePotions;
		for (int i = 0; i < this->AllPotions.Num(); ++i)
		{
			TArray<TSubclassOf<UIngredient>> PotionIngredients = this->AllPotions[i].GetDefaultObject()->GetIngredients();
			for (int k = 0; k < this->Ingredients.Num(); ++k)
			{
				if (PotionIngredients.Contains(this->Ingredients[k]))
				{
					PotionIngredients.Remove(this->Ingredients[k]);

					if (PotionIngredients.Num() <= 0)
					{
						break;
					}
				}
			}

			if (PotionIngredients.Num() <= 0)
			{
				ViablePotions.Add(this->AllPotions[i]);
			}
		}

		if (ViablePotions.Num() > 0)
		{
			TSubclassOf<UPotion> NewPotion = ViablePotions[FMath::RandRange(0, ViablePotions.Num() - 1)];

			TArray<TSubclassOf<UIngredient>> NewPotionIngredients = NewPotion.GetDefaultObject()->GetIngredients();
			for (int i = 0; i < NewPotionIngredients.Num(); ++i)
			{
				this->Ingredients.Remove(NewPotionIngredients[i]);
			}

			/// TODO: use excess ingredients

			MainCharacter->AddPotion(NewPotion, 1);
		}
	}

	this->Ingredients.Empty();
	this->OnIngredientsChanged.Broadcast(this);
}

void ACauldron::Explode()
{
	this->Ingredients.Empty();
	this->OnIngredientsChanged.Broadcast(this);

	this->GetWorld()->GetFirstPlayerController()->GetPawn()->TakeDamage(30.0f, FRadialDamageEvent(), this->GetInstigatorController(), this);
}
