// Copyright 2020 Andrew Joy, All Rights Reserved.

#include "LD46.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE(FDefaultGameModuleImpl, LD46, "LD46");
