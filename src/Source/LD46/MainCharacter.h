// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/Character.h"

#include "MainCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FMainCoinsChanged, AMainCharacter*, Sender);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FMainIngredientsChanged, AMainCharacter*, Sender);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FMainPotionsChanged, AMainCharacter*, Sender);

UCLASS()
class LD46_API AMainCharacter : public ACharacter
{
	GENERATED_BODY()
	
public:
	AMainCharacter();

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY(BlueprintAssignable, Category = Inventory)
	FMainCoinsChanged OnCoinsChanged;

	UPROPERTY(BlueprintAssignable, Category = Inventory)
	FMainIngredientsChanged OnIngredientsChanged;

	UPROPERTY(BlueprintAssignable, Category = Inventory)
	FMainPotionsChanged OnPotionsChanged;

	UFUNCTION(BlueprintCallable, Category = Inventory)
	void ChangeCoins(int32 DeltaCoins);

	UFUNCTION(BlueprintCallable, Category = Inventory)
	void AddIngredient(TSubclassOf<class UIngredient> IngredientClass, int32 Count);

	UFUNCTION(BlueprintCallable, Category = Inventory)
	bool RemoveIngredient(TSubclassOf<class UIngredient> IngredientClass, int32 Count);

	UFUNCTION(BlueprintCallable, Category = Inventory)
	void AddPotion(TSubclassOf<class UPotion> PotionClass, int32 Count);

	UFUNCTION(BlueprintCallable, Category = Inventory)
	bool RemovePotion(TSubclassOf<class UPotion> PotionClass, int32 Count);

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Inventory, meta = (AllowPrivateAccess = "true"))
	int32 Coins;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Inventory, meta = (AllowPrivateAccess = "true"))
	TArray<class UIngredient*> Ingredients;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Inventory, meta = (AllowPrivateAccess = "true"))
	TArray<class UPotion*> Potions;

public:
	FORCEINLINE int32 GetCoins() const { return this->Coins; }
	FORCEINLINE TArray<class UIngredient*> const& GetIngredients() const { return this->Ingredients; }
	FORCEINLINE TArray<class UPotion*> const& GetPotions() const { return this->Potions; }
};
