// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/PlayerController.h"

#include "MainPlayerController.generated.h"

UCLASS()
class LD46_API AMainPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	AMainPlayerController();

protected:
	virtual void SetupInputComponent() override;

	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION(BlueprintCallable, Category = Level)
	void OpenLevel(FName LevelName);

private:
	UPROPERTY(EditDefaultsOnly, Category = UI, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<class UFadeWidget> FadeWidgetClass;

	UPROPERTY()
	class UFadeWidget* FadeWidget;

	UPROPERTY(EditDefaultsOnly, Category = UI, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<class UHUDWidget> HUDWidgetClass;

	UPROPERTY()
	class UHUDWidget* HUDWidget;

	UPROPERTY()
	FName OpeningLevelName;

	void StartMouseCapture();
	void StopMouseCapture();

	UPROPERTY()
	uint32 bIsMouseCaptured : 1;
};
