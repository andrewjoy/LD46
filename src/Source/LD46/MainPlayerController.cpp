// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "MainPlayerController.h"

#include "Kismet/GameplayStatics.h"
#include "TimerManager.h"
#include "UObject/ConstructorHelpers.h"

#include "UI/FadeWidget.h"
#include "UI/HUDWidget.h"

AMainPlayerController::AMainPlayerController()
{
	struct FConstructorStatics
	{
		ConstructorHelpers::FClassFinder<UFadeWidget> FadeWidgetClass;
		ConstructorHelpers::FClassFinder<UHUDWidget> HUDWidgetClass;
		FConstructorStatics()
			: FadeWidgetClass(TEXT("WidgetBlueprint'/Game/Blueprints/UI/BP_FadeWidget.BP_FadeWidget_C'"))
			, HUDWidgetClass(TEXT("WidgetBlueprint'/Game/Blueprints/UI/BP_HUDWidget.BP_HUDWidget_C'"))
		{
		}
	} Assets;

	this->PrimaryActorTick.bCanEverTick = true;

	this->FadeWidgetClass = Assets.FadeWidgetClass.Class;
	this->HUDWidgetClass = Assets.HUDWidgetClass.Class;
}

void AMainPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	this->InputComponent->BindAction("MouseCapture", IE_Pressed, this, &AMainPlayerController::StartMouseCapture).bConsumeInput = false;
	this->InputComponent->BindAction("MouseCapture", IE_Released, this, &AMainPlayerController::StopMouseCapture).bConsumeInput = false;
}

void AMainPlayerController::BeginPlay()
{
	Super::BeginPlay();

	this->FadeWidget = NewObject<UFadeWidget>(this, this->FadeWidgetClass);
	this->FadeWidget->AddToViewport(1000);
	this->FadeWidget->FadeBackward();

	this->HUDWidget = NewObject<UHUDWidget>(this, this->HUDWidgetClass);
	this->HUDWidget->AddToViewport();

	this->bShowMouseCursor = true;

	FInputModeGameAndUI InputMode;
	InputMode.SetHideCursorDuringCapture(true);

	this->SetInputMode(InputMode);
}

void AMainPlayerController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (!this->bIsMouseCaptured)
	{
		this->SetControlRotation(FMath::Lerp(this->GetControlRotation(), FRotator(-40.0f, 0.0f, 0.0f), DeltaSeconds * 3.0f));
	}
}

void AMainPlayerController::OpenLevel(FName LevelName)
{
	if (this->OpeningLevelName.IsNone())
	{
		this->OpeningLevelName = LevelName;

		this->FadeWidget->FadeForward();

		FTimerHandle Handle;
		this->GetWorld()->GetTimerManager().SetTimer(Handle, [this, LevelName]() { UGameplayStatics::OpenLevel(this, LevelName); }, 1.5f, false);
	}
}

void AMainPlayerController::StartMouseCapture()
{
	this->bIsMouseCaptured = true;

	this->HUDWidget->SetVisibility(ESlateVisibility::Collapsed);
}

void AMainPlayerController::StopMouseCapture()
{
	this->bIsMouseCaptured = false;

	this->HUDWidget->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
}
