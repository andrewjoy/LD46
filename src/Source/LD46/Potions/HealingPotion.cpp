// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "HealingPotion.h"

#include "../Ingredients/WormwoodIngredient.h"
#include "../Ingredients/VolatilyIngredient.h"

UHealingPotion::UHealingPotion()
{
	this->Name = "Healing Potion";

	this->Ingredients.Add(UWormwoodIngredient::StaticClass());
	this->Ingredients.Add(UVolatilyIngredient::StaticClass());
}
