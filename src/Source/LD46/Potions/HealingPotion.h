// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "../Potion.h"

#include "HealingPotion.generated.h"

UCLASS()
class LD46_API UHealingPotion : public UPotion
{
	GENERATED_BODY()
	
public:
	UHealingPotion();
};
