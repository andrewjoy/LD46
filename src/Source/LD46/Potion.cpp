// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Potion.h"

UPotion::UPotion()
{
	this->Name = "Potion";
}

void UPotion::Use_Implementation()
{
	this->ChangeCount(-1);
}
