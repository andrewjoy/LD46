// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "MainCharacter.h"

#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

#include "Ingredient.h"
#include "Potion.h"

AMainCharacter::AMainCharacter()
{
	this->bUseControllerRotationYaw = false;

	this->GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	this->GetCharacterMovement()->GravityScale = 0.0f;
}

void AMainCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Look", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
}

void AMainCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	for (int i = 0; i < this->Ingredients.Num(); ++i)
	{
		this->Ingredients[i]->TickPrice(DeltaSeconds);
	}

	for (int i = 0; i < this->Potions.Num(); ++i)
	{
		this->Potions[i]->TickPrice(DeltaSeconds);
	}
}

void AMainCharacter::ChangeCoins(int32 DeltaCoins)
{
	this->Coins = FMath::Max(0, this->Coins + DeltaCoins);
	this->OnCoinsChanged.Broadcast(this);
}

void AMainCharacter::AddIngredient(TSubclassOf<UIngredient> IngredientClass, int32 Count)
{
	for (int i = 0; i < this->Ingredients.Num(); ++i)
	{
		if (this->Ingredients[i]->GetClass() == IngredientClass)
		{
			this->Ingredients[i]->ChangeCount(Count);
			this->OnIngredientsChanged.Broadcast(this);

			return;
		}
	}

	UIngredient* const NewIngredient = NewObject<UIngredient>(this, IngredientClass);
	NewIngredient->ChangeCount(Count);
	NewIngredient->TickPrice(0.0f);

	this->Ingredients.Add(NewIngredient);
	this->OnIngredientsChanged.Broadcast(this);
}

bool AMainCharacter::RemoveIngredient(TSubclassOf<UIngredient> IngredientClass, int32 Count)
{
	for (int i = 0; i < this->Ingredients.Num(); ++i)
	{
		if (this->Ingredients[i]->GetClass() == IngredientClass && this->Ingredients[i]->GetCount() >= Count)
		{
			this->Ingredients[i]->ChangeCount(-Count);
			this->OnIngredientsChanged.Broadcast(this);

			return true;
		}
	}

	return false;
}

void AMainCharacter::AddPotion(TSubclassOf<UPotion> PotionClass, int32 Count)
{
	for (int i = 0; i < this->Potions.Num(); ++i)
	{
		if (this->Potions[i]->GetClass() == PotionClass)
		{
			this->Potions[i]->ChangeCount(Count);
			this->OnPotionsChanged.Broadcast(this);

			return;
		}
	}

	UPotion* const NewPotion = NewObject<UPotion>(this, PotionClass);
	NewPotion->ChangeCount(Count);
	NewPotion->TickPrice(0.0f);

	this->Potions.Add(NewPotion);
	this->OnPotionsChanged.Broadcast(this);
}

bool AMainCharacter::RemovePotion(TSubclassOf<UPotion> PotionClass, int32 Count)
{
	for (int i = 0; i < this->Potions.Num(); ++i)
	{
		if (this->Potions[i]->GetClass() == PotionClass && this->Potions[i]->GetCount() >= Count)
		{
			this->Potions[i]->ChangeCount(-Count);
			this->OnPotionsChanged.Broadcast(this);

			return true;
		}
	}

	return false;
}
