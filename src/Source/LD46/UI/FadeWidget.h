// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "Blueprint/UserWidget.h"

#include "FadeWidget.generated.h"

UCLASS()
class LD46_API UFadeWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = Fade)
	void FadeForward();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = Fade)
	void FadeBackward();
};
