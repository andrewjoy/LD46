// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "Blueprint/UserWidget.h"

#include "HUDWidget.generated.h"

UCLASS()
class LD46_API UHUDWidget : public UUserWidget
{
	GENERATED_BODY()
};
