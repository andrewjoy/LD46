// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "Item.generated.h"

UENUM(BlueprintType)
enum class EItemRarity : uint8
{
	None = 0,
	Common = 1,
	Uncommon = 2,
	Rare = 3,
	Mythic = 4,
};

UCLASS(Blueprintable)
class LD46_API UItem : public UObject
{
	GENERATED_BODY()
	
public:
	UItem();

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Item)
	FString Name;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Item)
	EItemRarity Rarity;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Item)
	int32 BasePrice;

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Item, meta = (AllowPrivateAccess = "true"))
	float Price;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Item, meta = (AllowPrivateAccess = "true"))
	int32 Count;

public:
	UFUNCTION(BlueprintCallable, Category = Item)
	void TickPrice(float DeltaSeconds);

	UFUNCTION(BlueprintCallable, Category = Item)
	void ChangeCount(int32 DeltaCount);

	UFUNCTION(BlueprintCallable, Category = Item)
	void Sell();

	FORCEINLINE FString const& GetName() const { return this->Name; }
	FORCEINLINE int32 GetBasePrice() const { return this->BasePrice; }
	FORCEINLINE float GetPrice() const { return this->Price; }
	FORCEINLINE int32 GetCount() const { return this->Count; }
};
